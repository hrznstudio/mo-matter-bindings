// Resources
registerMatter(<ore:ingotIron>, [<type:metallic> * 9]);
registerMatter(<ore:ingotGold>, [<type:metallic> * 18]);
registerMatter(<ore:ingotCopper>, [<type:metallic> * 9]);
registerMatter(<ore:ingotTin>, [<type:metallic> * 9]);

registerMatter(<ore:gemDiamond>, [<type:precious> * 9]);
registerMatter(<ore:gemEmerald>, [<type:precious> * 18]);
registerMatter(<ore:gemQuartz>, [<type:precious> * 4]);
registerMatter(<ore:gemLapis>, [<type:precious> * 1]);
registerMatter(<minecraft:coal>, [<type:precious> * 2]);
registerMatter(<ore:dustRedstone>, [<type:earth> * 6, <type:precious> * 1]);
registerMatter(<ore:dustGlowstone>, [<type:nether> * 6, <type:precious> * 1]);

registerMatter(<minecraft:coal_ore>, [<type:precious> * 16]);
registerMatter(<minecraft:gold_ore>, [<type:metallic> * 90]);
registerMatter(<minecraft:iron_ore>, [<type:metallic> * 45]);
registerMatter(<minecraft:emerald_ore>, [<type:precious> * 90]);
registerMatter(<minecraft:diamond_ore>, [<type:precious> * 45]);
registerMatter(<minecraft:redstone_ore>, [<type:earth> * 36, <type:precious> * 6]);
registerMatter(<minecraft:quartz_ore>, [<type:precious> * 36]);
registerMatter(<minecraft:lapis_ore>, [<type:precious> * 10]);

registerMatter(<minecraft:lava_bucket>, [<type:metallic> * 24, <type:earth> * 8]);
registerMatter(<minecraft:water_bucket>, [<type:metallic> * 24, <type:nether> * 8]);
registerMatter(<minecraft:milk_bucket>, [<type:metallic> * 24, <type:organic> * 8]);


//Random Blocks
registerMatter(<ore:dirt>, [<type:earth> * 2, <type:organic> * 1]);
registerMatter(<minecraft:dirt:2>, [<type:earth> * 2, <type:organic> * 1]);
registerMatter(<ore:blockWool>, [<type:organic> * 8]);
registerMatter(<ore:blockCloth>, [<type:organic> * 8]);
registerMatter(<ore:blockGlass>, [<type:earth> * 6]);
registerMatter(<ore:cobblestone>, [<type:earth> * 4]);
registerMatter(<minecraft:gravel>, [<type:earth> * 4]);
registerMatter(<minecraft:flint>, [<type:earth> * 12]);
registerMatter(<ore:logWood>, [<type:organic> * 16]);
registerMatter(<ore:sand>, [<type:earth> * 4]);
registerMatter(<minecraft:clay_ball>, [<type:earth> * 2]);
registerMatter(<minecraft:end_stone>, [<type:ender> * 10]);
registerMatter(<ore:stone>, [<type:earth> * 4]);
registerMatter(<minecraft:soul_sand>,[<type:earth> * 4, <type:nether> * 4]);
registerMatter(<minecraft:snowball>, [<type:earth> * 2]);
registerMatter(<minecraft:obsidian>, [<type:earth> * 32]);
registerMatter(<minecraft:mycelium>, [<type:earth> * 2, <type:organic> * 2]);
registerMatter(<minecraft:ice>, [<type:earth> * 6]);
registerMatter(<minecraft:packed_ice>, [<type:earth> * 12]);
registerMatter(<minecraft:bedrock>, [<type:earth> * 1024]);
registerMatter(<minecraft:sponge>, [<type:earth> * 8, <type:organic> * 1]);

registerMatter(<minecraft:mossy_cobblestone>, [<type:earth> * 4, <type:organic> * 4]);
registerMatter(<minecraft:netherrack>, [<type:nether> * 2]);
registerMatter(<minecraft:web>, [<type:organic> * 4]);

registerMatter(<minecraft:command_block>, [<type:quantum> * 10]);

//Plant stuff
registerMatter(<ore:treeLeaves>, [<type:organic> * 2]);
registerMatter(<minecraft:grass>, [<type:earth> * 2, <type:organic> * 2]);
registerMatter(<minecraft:cactus>, [<type:organic> * 6]);
registerMatter(<minecraft:pumpkin>, [<type:organic> * 6]);
registerMatter(<minecraft:vine>, [<type:organic> * 2]);
registerMatter(<minecraft:chorus_fruit>, [<type:ender> * 5, <type:organic> * 10]);
registerMatter(<minecraft:wheat_seeds>, [<type:organic> * 3]);
registerMatter(<minecraft:brown_mushroom>, [<type:organic> * 3]);
registerMatter(<minecraft:red_flower:*>, [<type:organic> * 4]);
registerMatter(<minecraft:yellow_flower:*>, [<type:organic> * 4]);
registerMatter(<minecraft:melon>, [<type:organic> * 4]);
registerMatter(<minecraft:red_mushroom>, [<type:organic> * 3]);
registerMatter(<ore:treeSapling>, [<type:organic> * 2]);
registerMatter(<minecraft:chorus_plant>, [<type:organic> * 4, <type:ender> * 2]);
registerMatter(<minecraft:wheat>, [<type:organic> * 5]);
registerMatter(<minecraft:potato>, [<type:organic> * 4]);
registerMatter(<minecraft:beetroot>, [<type:organic> * 5]);
registerMatter(<minecraft:reeds>, [<type:organic> * 5]);
registerMatter(<minecraft:deadbush>, [<type:organic> * 2]);
registerMatter(<minecraft:tallgrass:9>, [<type:organic> * 2]);
registerMatter(<minecraft:apple>, [<type:organic> * 2]);
registerMatter(<minecraft:waterlily>, [<type:organic> * 3]);
registerMatter(<minecraft:nether_wart>, [<type:organic> * 2, <type:nether> * 2]);
registerMatter(<minecraft:carrot>, [<type:organic> * 4]);
registerMatter(<minecraft:beetroot_seeds>, [<type:organic> * 3]);
registerMatter(<minecraft:double_plant:*>, [<type:organic> * 3]);
registerMatter(<minecraft:chorus_flower>, [<type:organic> * 4, <type:ender> * 2]);
registerMatter(<minecraft:poisonous_potato>, [<type:organic> * 1]);

//Mob drops
registerMatter(<minecraft:skull:5>, [<type:ender> * 20]);
registerMatter(<minecraft:dragon_egg>, [<type:ender> * 100, <type:quantum> * 10]);
registerMatter(<minecraft:ghast_tear>, [<type:nether> * 4]);
registerMatter(<minecraft:wool>, [<type:organic> * 6]);
registerMatter(<ore:enderpearl>, [<type:ender> * 8]);
registerMatter(<minecraft:rabbit_hide>, [<type:organic> * 6]);
registerMatter(<minecraft:blaze_rod>, [<type:organic> * 6, <type:nether> * 6]);
registerMatter(<minecraft:rabbit>, [<type:organic> * 6]);
registerMatter(<minecraft:slime_ball>, [<type:organic> * 6]);
registerMatter(<minecraft:beef>, [<type:organic> * 6]);
registerMatter(<minecraft:totem_of_undying>, [<type:organic> * 16, <type:quantum> * 8]);
registerMatter(<minecraft:mutton>, [<type:organic> * 6]);
registerMatter(<minecraft:nether_star>, [<type:nether> * 100, <type:quantum> * 10]);
registerMatter(<minecraft:chicken>, [<type:organic> * 6]);
registerMatter(<minecraft:feather>, [<type:organic> * 4]);
registerMatter(<minecraft:spider_eye>, [<type:organic> * 4]);
registerMatter(<minecraft:bone>, [<type:organic> * 2]);
registerMatter(<minecraft:fish:*>, [<type:organic> * 5]);
registerMatter(<minecraft:shulker_shell>, [<type:organic> * 12, <type:ender> * 16]);
registerMatter(<minecraft:prismarine_shard>, [<type:organic> * 8]);
registerMatter(<minecraft:prismarine_crystals>, [<type:organic> * 8]);
registerMatter(<minecraft:rabbit_foot>, [<type:organic> * 8]);
registerMatter(<minecraft:rotten_flesh>, [<type:organic> * 2]);
registerMatter(<minecraft:dye:0>, [<type:organic> * 3]);
registerMatter(<minecraft:dye:3>, [<type:organic> * 3]);
registerMatter(<ore:record>, [<type:quantum> * 4]);
registerMatter(<minecraft:gunpowder>, [<type:organic> * 4]);
registerMatter(<minecraft:porkchop>, [<type:organic> * 6]);
registerMatter(<minecraft:egg>, [<type:organic> * 2]);
registerMatter(<minecraft:string>, [<type:organic> * 2]);